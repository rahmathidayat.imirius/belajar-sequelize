"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here. */

    await queryInterface.bulkInsert(
      "Charas",
      [
        {
          name: "Naruto",
          element: "Angin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Sasuke",
          element: "Petir",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Itachi",
          element: "Api",
          createdAt: new Date(),
          updatedAt: new Date(),
        },

      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     */

    await queryInterface.bulkDelete("Charas", null, {});
  },
};
